# erlang-colorgrid #

### wut? ###
My first Erlang project. It generates two-dimensional grids of random colors and pukes them into your terminal. It's great fun.

### Dependencies ###
[erlang-color](https://github.com/julianduque/erlang-color) by Julian Duque

### Installation ###
* install erlang (can you run `erl` on the command line?)
* clone the dependencies
* clone this repo
* cd into this repo's clone
* run `ERL_LIBS=/full/path/to/dependency/root erl` (this makes the dependency visible to erl)

> c(color).

> c(grid).

> grid:grid(grid:random([10, 10]).

you should see a colorful grid.

### Options ###
The main function that is outputted is grid:random/2. Here's how it works.

To get 10x10 grid of random 8-bit colors, run
> grid:grid(grid:random([10, 10])).

or just
> grid:grid(grid:random(10)).

The list passed into random is your X, Y size. [10, 10] means 10 columns, 10 rows.

To get 10x10 grid of random 16-bit colors, run
> grid:grid(grid:random([10, 10], 16)).

The only difference is that we pass the integer 16 as a second argument. This invokes grid:random/2.

To get a 32-bit colors space, just pass 32 instead of 16.