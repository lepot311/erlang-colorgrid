-module('grid').
-export([grid/1]).
-export([random/1, random/2]).

-define(FILL, "  "). % what is rendered in a char (grid cell)

%8-bit color mapping
-define(COLORS, [
    { 1, "black"   },
    { 2, "red"     },
    { 3, "green"   },
    { 4, "yellow"  },
    { 5, "blue"    },
    { 6, "magenta" },
    { 7, "cyan"    },
    { 8, "white"   }
]).

random([X, Y]) -> random([X, Y], 8);
random(N)      -> random([N, N]).

random([X, Y], 8)  -> grid(fun random8/0,  X, Y);
random([X, Y], 16) -> grid(fun random16/0, X, Y);
random([X, Y], 32) -> grid(fun random32/0, X, Y).

%helpers
grid(F, X, Y) ->
    %apply a function across a 2-dimensional matrix
    [ [ F() || _ <- lists:seq(1, X) ] 
            || _ <- lists:seq(1, Y) ].

triplet(F, [Args]) ->
    %apply a function and its Args three times
    [ F(Args) || _ <- lists:seq(1, 3) ].

hex(Int) ->
    %right-align the hex value of int and pad with zero
    string:right(integer_to_list(Int, 16), 2, $0).

%color fetchers
random8() ->
    %a random int between 1 and 8 inclusive
    random:uniform(8).

random16() ->
    %a triplet of a random int between 1 and 5 inclusive
    triplet(fun random:uniform/1, [5]).

random32() ->
    %a triplet of a random int between 1 and 255 inclusive
    triplet(fun round/1, [random:uniform() * 255]).

%functions for composing the grid of characters and rows
grid([]) ->
    io:format("~n");
grid([ Head | Tail ]) ->
    row(Head),  %print out the row
    grid(Tail). %recurse with rest grid

row([]) ->
    io:format("~n");
row([ Head | Tail ]) ->
    char(Head), %print out the char
    row(Tail).  %recurse with rest of row

%functions for printing individual characters
%8-bit color
char(Char) when is_integer(Char), Char < 9 ->
    io:format("~s", [erlang:apply(
                       color,
                       %call the appropriate color function from COLORS mapping
                       list_to_atom("on_"++proplists:get_value(Char, ?COLORS)),
                       [?FILL])
                    ]);
%16-bit color
char([R, G, B]) when R<6, G<6, B<6 ->
    io:format("~s", [color:rgb([R, G, B], ?FILL)]);
%32-bit color
char([R, G, B]) ->
    %combine the hex value of each RGB channel into a single string
    io:format("~s", [erlang:apply(
                       color,
                       true,
                       [hex(R) ++ hex(G) ++ hex(B), ?FILL])
                    ]).
